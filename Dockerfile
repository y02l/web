FROM node:14.7.0-alpine3.12 AS dev
WORKDIR ./app
COPY app/package.json .
RUN npm i
COPY app/ .
RUN npm run build
ENTRYPOINT ["npm", "run", "start"]

FROM nginx:1.18-alpine AS prod
ADD "https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h" skipcache
COPY nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=dev /app/dist /usr/share/nginx/html
