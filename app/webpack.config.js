const webpack = require('webpack');
var path = require('path');

module.exports = {
  entry: {
    index: './src/index.tsx',
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loaders: ['babel-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.tsx?$/,
        loaders: ['ts-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.css?$/,
        loaders: ['style-loader', 'css-loader'],
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
    modules: ['node_modules']
  },
  devServer: {
    port: 8080,
    host: '0.0.0.0',
    historyApiFallback: true,
    contentBase: path.join(__dirname, 'dist'),
    clientLogLevel: 'info',
    proxy: {
      '/api/hello-flask': {
	target: 'http://hello-flask',
        changeOrigin: true,
        pathRewrite: {'^/api/hello-flask': ''}
      },
      '/api/deck-rule': {
	target: 'http://deck-rule',
        changeOrigin: true,
        pathRewrite: {'^/api/deck-rule': ''}
      },
      '/api/card-db': {
	target: 'http://card-db',
        changeOrigin: true,
        pathRewrite: {'^/api/card-db': ''}
      },
      '/api/deck-to-cards': {
	target: 'http://deck-to-cards',
        changeOrigin: true,
        pathRewrite: {'^/api/deck-to-cards': ''}
      }
    }
  },
  watchOptions: {
    ignored: /node_modules/
  }
};
