import * as React from 'react'
import { Route } from 'react-router-dom'

import { RulePage } from '../rule'
import { CheckerPage } from '../checker'

const Checker: React.FC = () => (
  <CheckerPage />
)
const Rules: React.FC = () => (
  <RulePage />
)

export const Router: React.FC = () => {
  return (
    <>
      <Route exact path='/' component={Checker} />
      <Route path='/checker' component={Checker} />
      <Route path='/rules' component={Rules} />
    </>
  )
}
