import * as React from 'react'
import axios from 'axios'

import { Form, Spinner } from 'react-bootstrap'

import { Cards } from './cards'
import { useDeckToCards } from './useDeckToCards'

export const CheckerPage = () => {
  const deckToCardsHooks = useDeckToCards()

  return (
    <>
      <Form>
        <Form.Group>
          <Form.Label>スマホ版のデッキのスクショをアップロード</Form.Label>
          <Form.File onChange={(e) => { deckToCardsHooks.setFile(e.target.files[0]) }} />
        </Form.Group>
      </Form>

      <Spinner animation="border" role="status" hidden={!deckToCardsHooks.whileDeckToCards}>
        <span className="sr-only">Loading...</span>
      </Spinner>

      <Cards cards={deckToCardsHooks.cardNames} />

      <img src={deckToCardsHooks.imageUri} />
    </>
  )
}
