import * as React from 'react'

import { Button, Modal } from 'react-bootstrap'

import { CardsHooks, Card } from './useCards'
import { CardForm } from './cardForm'

type Props = {
  cardsHooks: CardsHooks
  card: Card
  show: boolean
  handleClose: () => void
}

export const EditCardModal: React.FC<Props> = props => {
  const [name, setName] = React.useState(props.card.name)


  return (
    <>
      <Modal show={props.show} onHide={props.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>カードを編集します</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <CardForm name={name} onChange={setName} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={props.handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={() => {
            props.cardsHooks.updateCard(props.card.id, name)
            props.handleClose()
          }}>
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}
