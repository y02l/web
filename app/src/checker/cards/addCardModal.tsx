import * as React from 'react'

import { Button, Modal } from 'react-bootstrap'

import { CardsHooks } from './useCards'
import { CardForm } from './cardForm'

type Props = {
  cardsHooks: CardsHooks
  show: boolean
  handleClose: () => void
}

export const AddCardModal: React.FC<Props> = props => {
  const [name, setName] = React.useState('')

  return (
    <>
      <Modal show={props.show} onHide={props.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>カードを追加します</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <CardForm name={name} onChange={setName} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={props.handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={() => {
            props.cardsHooks.addCard(name)
            props.handleClose()
          }}>
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}
