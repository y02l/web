import * as React from 'react'
import axios from 'axios'

import { Form } from 'react-bootstrap'

type Rules = {
  _id: string,
  name: string,
  costs: {
    name: string, cost: number
  }[]
}[]

type Props = {
  onChange: (ruleid: string) => void
}

export const RuleSelecor: React.FC<Props> = (props) => {
  const [rules, setRules] = React.useState<Rules>([])
  const [selected, setSelected] = React.useState<string>('')

  React.useEffect(() => {
    axios.get('/api/deck-rule/rules').then((res) => {
      const data: Rules = res.data
      setRules(data)
      setSelected(data[0]._id)
    })
  }, [])

  React.useEffect(() => {
    props.onChange(selected)
  }, [selected])

  return (
    <Form>
      <Form.Group controlId="exampleForm.SelectCustom">
        <Form.Label>ルール</Form.Label>
        <Form.Control as="select" custom
          value={selected}
          onChange={(e) => { setSelected(e.target.value) }}
        >
          {rules.map((e) => (
            <option value={e._id} key={e._id}>{e.name}</option>
          ))}
        </Form.Control>
      </Form.Group>
    </Form>
  )

}
