import * as React from 'react'
import axios from 'axios'

export interface Card {
  id: number
  name: string
  cost: number
  is02: boolean
}

export interface Name {
  id: number
  name: string
}

export interface Rule {
  costs: {
    name: string
    cost: number
  }[]
}

export interface CardsHooks {
  setCardNames: (cards: string[]) => void
  addCard: (name: string) => void
  deleteCard: (id: number) => void
  updateCard: (id: number, name: string) => void
  setRuleId: (ruleid: string) => void
}

const calcCost = (rule: Rule, name: string): number => {
  const d = rule.costs.filter((cost) => (cost.name === name))
  return d.length === 0 ? 0 : d[0].cost
}

const getIs02 = async (name: string): Promise<boolean> => {
  return (await axios.get('/api/card-db/card/' + name)).data.is02
}

export const useCards = (): [Card[], CardsHooks] => {
  const [rule, setRule] = React.useState<Rule>({ costs: [] })
  const [names, setNames] = React.useState<Name[]>([])
  const [cards, setCards] = React.useState<Card[]>([])

  React.useEffect(() => {
    const updateCards = async () => {
      const newCards: Card[] = await Promise.all(
        names.map(async (name) => {
          const is02 = await getIs02(name.name)
          const cost = calcCost(rule, name.name)
          return { ...name, cost, is02 }
        }))
      setCards(newCards)
    }
    updateCards()
  }, [rule, names])

  const setRuleId = (ruleid: string) => {
    axios.get('/api/deck-rule/rules').then((res) => {
      const data: { _id: string, costs: { name: string, cost: number }[] }[] = res.data
      const costs = data.filter((e) => (e._id === ruleid))
      if (costs.length === 0)
        return
      setRule({ costs: costs[0].costs })
    })
  }

  const setCardNames = (names: string[]) => {
    setNames(names.map((name, index) => ({ name, id: index })))
  }

  const addCard = (name) => {
    setNames((prev) => {
      const newName = {
        name,
        id: prev.length === 0 ? 0 : prev.reduce((a, c) => Math.max(a, c.id), 0) + 1,
      }
      return [newName, ...prev]
    })
  }

  const deleteCard = (id: number) => {
    setNames((prev) => (
      prev.filter((e) => (e.id !== id))
    ))
  }

  const updateCard = (id: number, name: string) => {
    setNames((prev) => (
      prev.map((e) => (
        e.id !== id ? e : { ...e, name }
      ))
    ))
  }

  return [cards, { addCard, deleteCard, updateCard, setRuleId, setCardNames }]
}
