import * as React from 'react'
import { Form } from 'react-bootstrap'

import { CardSelector } from '../../card_selector'

type Props = {
  name: string
  onChange: (name: string) => void
}

export const CardForm: React.FC<Props> = props => {
  const [name, setName] = React.useState('')

  const handleChangeName = (name: string) => {
    setName(name)
    props.onChange(name)
  }

  return (
    <Form onSubmit={e => e.preventDefault()}>
      <Form.Label>修正前:{props.name}</Form.Label>
      <Form.Group>
        <CardSelector
          placeholder='カード名'
          text={name}
          onChange={handleChangeName}
          requestParam={{ only02: 'false' }} />
      </Form.Group>
    </Form >
  )
}
