import * as React from 'react'

import { Button, Badge, Card as CardBS } from 'react-bootstrap'
import { FaEdit } from 'react-icons/fa'
import { RiDeleteBin2Line } from 'react-icons/ri'
import { CardsHooks, Card } from './useCards'
import { EditCardModal } from './editCardModal'

import { ConfirmModal } from '../../confirm_modal'
import { useConfirmModal } from '../../confirm_modal/useConfirmModal'

type Props = {
  cardsHooks: CardsHooks
  card: Card
}

export const CostRow: React.FC<Props> = props => {
  const [editing, setEditing] = React.useState(false)
  const confirmModalHooks = useConfirmModal()

  const nopadding_style = {
    paddingTop: 1,
    paddingBottom: 1,
    verticalAlign: 'middle',
    fontSize: 14
  }

  return (
    <CardBS>
      <CardBS.Body style={nopadding_style} >
        {props.card.name}
        <Badge variant="secondary" hidden={props.card.cost === 0}>cost: {props.card.cost}</Badge>
        <Badge variant="danger" hidden={props.card.is02}>NG</Badge>

        <Button variant="outline-dark" size="sm" style={{ ...nopadding_style, float: 'right' }}
          onClick={confirmModalHooks.show}>
          <RiDeleteBin2Line />
        </Button>
        <ConfirmModal
          title="削除しますか?"
          confirmModalHooks={confirmModalHooks}
          onConfirm={() => { props.cardsHooks.deleteCard(props.card.id) }}
          onCancel={() => { }}
        >
          {props.card.name}
        </ConfirmModal>

        <Button variant="outline-dark" size="sm" style={{ ...nopadding_style, float: 'right' }}
          onClick={() => { setEditing(true) }}>
          <FaEdit />
        </Button>
        <EditCardModal card={props.card} cardsHooks={props.cardsHooks}
          show={editing} handleClose={() => { setEditing(false) }} />

      </CardBS.Body >
    </CardBS >
  )
}
