import * as React from 'react'

import { Badge, Button } from 'react-bootstrap'
import { CgPlayListAdd } from 'react-icons/cg'

import { useCards } from './useCards'
import { RuleSelecor } from './RuleSelector'
import { CostRow } from './CostRow'
import { AddCardModal } from './addCardModal'

type Props = {
  cards: string[]
}

export const Cards: React.FC<Props> = (props) => {
  const [cards, cardsHooks] = useCards()
  const [showAddModal, setShowAddModal] = React.useState(false)

  React.useEffect(() => {
    cardsHooks.setCardNames(props.cards)
  }, [props.cards])

  const cardNum = cards.length
  const totalCost = cards.reduce((p, c) => (Number(p) + Number(c.cost)), 0)
  const is02 = cards.reduce((p, c) => (p && c.is02), true)

  return (
    <>
      <RuleSelecor onChange={(ruleid: string) => { cardsHooks.setRuleId(ruleid) }} />

      <div>
        <Button variant="outline-dark" size="sm"
          onClick={() => { setShowAddModal(true) }}>
          <CgPlayListAdd />
        </Button>
        <AddCardModal cardsHooks={cardsHooks}
          show={showAddModal} handleClose={() => { setShowAddModal(false) }} />

        <Badge variant={cardNum >= 20 ? "success" : "danger"}>{cardNum}枚</Badge>
        <Badge variant={totalCost <= 10 ? "success" : "danger"}>コスト{totalCost}</Badge>
        <Badge variant={is02 ? "success" : "danger"}>02:{is02 ? "OK" : "NG"}</Badge>

        {cards.map((e) => (
          <CostRow cardsHooks={cardsHooks} card={e} key={e.id} />
        ))}
      </div>
    </>
  )
}
