import * as React from 'react'
import axios from 'axios'
import Resizer from 'react-image-file-resizer'

export interface DeckToCardsHooks {
  imageUri: string
  cardNames: string[]
  whileDeckToCards: boolean
  setFile: (dile: object) => void
}

export const useDeckToCards = (): DeckToCardsHooks => {
  const [imageUri, setImageUri] = React.useState('')
  const [cardNames, setCardNames] = React.useState<string[]>([])
  const [whileDeckToCards, setWhileDeckToCards] = React.useState(false)

  const setFile = (file: Blob) => {
    setImageUri('')
    setCardNames([])
    Resizer.imageFileResizer(file, 500, 500, 'PNG', 100, 0,
      uri => {
        if ('string' === typeof uri) {
          setImageUri(uri)
          deckToCards(uri)
        }
      },
      'base64'
    )
  }

  const deckToCards = (imageUri: string) => {
    setWhileDeckToCards(true)
    axios.post('/api/deck-to-cards/', { q: imageUri }, {
      headers: {
        'Content-Type': 'application/json'
      }
    }).then((res) => {
      setCardNames(res.data)
    }).catch(() => {
      setCardNames([])
    }).finally(() => {
      setWhileDeckToCards(false)
    })
  }

  return { imageUri, cardNames, whileDeckToCards, setFile }
}
