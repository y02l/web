import * as React from 'react'
import Autosuggest from 'react-autosuggest'
import axios from 'axios'

import './autosuggest.css'

type Props = {
  placeholder: string,
  text: string,
  requestParam: object
  onChange: (text: string) => void
}

export const CardSelector = (props: Props) => {
  const [suggestions, setSuggestions] = React.useState([])
  const [isLoading, setIsLoading] = React.useState(false)

  const onChange = (event, { newValue, method }) => {
    props.onChange(newValue)
  }
  const onSuggestionsFetchRequested = async ({ value, reason }) => {
    setIsLoading(true)
    const url = '/api/card-db/search?q=' + value + '&size=20'
    const param = Object.keys(props.requestParam).map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(props.requestParam[key])
    }).join('&');
    const resp = await axios.get(url + '&' + param)

    setSuggestions(resp.data['data'])
    setIsLoading(false)
  }
  const onSuggestionsClearRequested = () => { setSuggestions([]) }
  const getSuggestionValue = (suggestion) => (suggestion)
  const renderSuggestion = (suggestion, { query, isHighlighted }) => {
    return <span>{suggestion}</span>
  }

  return (
    <Autosuggest
      suggestions={suggestions}
      onSuggestionsFetchRequested={onSuggestionsFetchRequested}
      onSuggestionsClearRequested={onSuggestionsClearRequested}
      getSuggestionValue={getSuggestionValue}
      renderSuggestion={renderSuggestion}
      inputProps={{
        placeholder: props.placeholder,
        value: props.text,
        onChange: onChange
      }}
    />
  )
}
