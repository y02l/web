import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.min.css'

import { Header } from './header'
import { Router } from './router'

ReactDOM.render(
  <>
    <BrowserRouter>
      <Header />
      <Router />
    </BrowserRouter>
  </>,
  document.getElementById('app')
)
