import * as React from 'react'

import { Button } from 'react-bootstrap'

import { RulesContext } from './index'
import { RulesHook } from './RulesHooks'

export const AddRuleButton = () => {
  const Rules = React.useContext<RulesHook>(RulesContext)

  return (
    <Button
      variant="secondary"
      onClick={() => { Rules.addRule({}) }}
    >
      新規ルール追加
    </Button>
  )
}
