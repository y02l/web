import * as React from 'react'

import _ from 'lodash'
import { Table, Card } from 'react-bootstrap'

import { Cost } from '../../RulesHooks'
import { AddCostButton } from './addCostButton'
import { DeleteCostButton } from './deleteCostButton'
import { EditCostModal } from './editCostModal'
import { IsCorrectNameBadge } from './isCorrectNameBadge'

type Props = {
  ruleid: number, costs: Cost[]
}

type CostsGroupByCost = {
  cost: number,
  cards: Cost[]
}[]

export const Costs: React.FC<Props> = props => {
  const [costsGroupByCost, setCostsGroupByCost] = React.useState<CostsGroupByCost>([])

  // costsgroupbycostの作成
  React.useEffect(() => {
    const group = Object.entries(_.groupBy(props.costs, c => (c.cost)))
      .sort((a: [string, Cost[]], b: [string, Cost[]]) => (- parseInt(a[0]) + (parseInt(b[0]))))
      .map((e: [string, Cost[]]) => (
        { cost: parseInt(e[0]), cards: e[1] }
      ))
    setCostsGroupByCost(group)
  }, [props.costs])

  return (
    <>
      <AddCostButton ruleid={props.ruleid} />

      {costsGroupByCost.map((e) => (
        <Card key={e.cost}>
          <Card.Header style={{ paddingTop: 1, paddingBottom: 1 }}>
            {e.cost}
          </Card.Header>
          <Card.Body style={{ paddingTop: 1, paddingBottom: 1 }}>
            {e.cards.map((card) => (
              <Card key={card.id}>
                <Card.Body style={{ paddingTop: 1, paddingBottom: 1 }}>
                  {card.name}
                  <IsCorrectNameBadge name={card.name} />
                  <DeleteCostButton ruleid={props.ruleid} costid={card.id} />
                  <EditCostModal ruleid={props.ruleid} cost={card} />
                </Card.Body>
              </Card>
            ))}
          </Card.Body>
        </Card>
      ))}
    </>
  )
}
