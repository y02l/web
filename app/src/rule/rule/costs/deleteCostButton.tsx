import * as React from 'react'

import { Button } from 'react-bootstrap'
import { RiDeleteBin2Line } from 'react-icons/ri'

import { RulesContext } from '../../index'
import { RulesHook } from '../../RulesHooks'

export const DeleteCostButton: React.FC<{ ruleid: number, costid: number }> = (props) => {
  const Rules = React.useContext<RulesHook>(RulesContext)

  return (
    <Button variant="outline-dark" size="sm" style={{float: 'right'}}
	    onClick={() => { Rules.deleteCost(props.ruleid, props.costid) }} >
      <RiDeleteBin2Line />
    </Button>
  )
}
