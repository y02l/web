import * as React from 'react'

import { Button, Modal } from 'react-bootstrap'
import { FaEdit } from 'react-icons/fa'

import { RulesContext } from '../../index'
import { RulesHook, Cost } from '../../RulesHooks'
import { EditCostForm } from './editCostForm'

type Props = {
  ruleid: number
  cost: Cost
}

export const EditCostModal: React.FC<Props> = props => {
  const [name, setName] = React.useState(props.cost.name)
  const [cost, setCost] = React.useState(props.cost.cost)

  const [show, setShow] = React.useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const Rules = React.useContext<RulesHook>(RulesContext)

  return (
    <>
      <Button variant="outline-dark" size="sm"  style={{float: 'right'}}
	      onClick={handleShow}>
	<FaEdit />
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>コストを編集します</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <EditCostForm ruleid={props.ruleid} costid={props.cost.id} name={props.cost.name} cost={props.cost.cost} setCost={setCost} setName={setName} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={() => {
            Rules.updateCost(props.ruleid, props.cost.id, { name, cost })
            handleClose()
          }}>
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}
