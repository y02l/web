import * as React from 'react'

import { Button } from 'react-bootstrap'

import { RulesContext } from '../../index'
import { RulesHook } from '../../RulesHooks'

export const AddCostButton: React.FC<{ ruleid: number }> = (props) => {
  const Rules = React.useContext<RulesHook>(RulesContext)

  return (
    <Button
      onClick={() => { Rules.addCost(props.ruleid) }}
    >
      コスト追加
    </Button>
  )
}
