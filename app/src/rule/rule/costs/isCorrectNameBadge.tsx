import * as React from 'react'
import axios from 'axios'

import { Badge } from 'react-bootstrap'

export const IsCorrectNameBadge: React.FC<{ name: string }> = props => {
  const [isCorrect, setIsCorrect] = React.useState(true)

  console.log(props.name)

  React.useEffect(() => {
    const checkAndSetIsCorrect = (name: string) => {
      axios.get('/api/card-db/card/' + name).then(resp => {
        setIsCorrect(resp.status == 200)
      }).catch(resp => {
        setIsCorrect(resp.status == 200)
      })
    }
    checkAndSetIsCorrect(props.name)
  }, [props.name])

  return (
    <Badge variant="danger" hidden={isCorrect}> 正しくないカード名です</Badge >
  )
}
