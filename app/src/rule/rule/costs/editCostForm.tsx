import * as React from 'react'
import { Button, Form } from 'react-bootstrap'

import { CardSelector } from '../../../card_selector'

type Props = {
  ruleid: number
  costid: number
  name: string
  cost: number
  setName: (name: string) => void
  setCost: (cost: number) => void
}

export const EditCostForm: React.FC<Props> = props => {
  const [name, setName] = React.useState(props.name)

  const handleChangeName = (name: string) => {
    setName(name)
    props.setName(name)
  }

  return (
    <Form onSubmit={e => e.preventDefault()}>
      <Form.Group>
        <CardSelector
          placeholder='カード名'
          text={name}
          onChange={handleChangeName}
          requestParam={{ only02: 'true' }} />
      </Form.Group>
      <Form.Group>
        <Form.Label>コスト</Form.Label>
        <Form.Control as="select" defaultValue={props.cost}
          onChange={(e) => { props.setCost(e.target.value) }}>
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
        </Form.Control>
      </Form.Group>
    </Form >
  )
}
