import * as React from 'react'

import { Button, Form, Col } from 'react-bootstrap'
import { MdDone } from 'react-icons/md'

type Props = {
  name: string,
  handleApply: (name: string) => void
}

export const EditRuleForm: React.FC<Props> = props => {
  const [name, setName] = React.useState(props.name)
  return (
    <Form.Row>
      <Col xs="10">
        <Form.Control type="text" placeholder="新しいルール名"
          onChange={(e) => { setName(e.target.value) }} />
      </Col>
      <Col xs="2">
        <Button variant="outline-dark" type="button" onClick={(e) => { props.handleApply(name) }}>
          <MdDone />
        </Button>
      </Col>
    </Form.Row>
  )
}
