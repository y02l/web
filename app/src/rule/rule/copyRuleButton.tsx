import * as React from 'react'

import { Button } from 'react-bootstrap'

import { RulesContext } from '../index'
import { RulesHook } from '../RulesHooks'

type PropsType = {
  id: number
}

export const CopyRuleButton: React.FC<PropsType> = (props) => {
  const Rules = React.useContext<RulesHook>(RulesContext)

  return (
    <>
      <Button onClick={() => { Rules.copyRule(props.id) }} >
        ルールコピー
      </Button>
    </>
  )
}
