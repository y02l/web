import * as React from 'react'

import { Button } from 'react-bootstrap'

import { RulesContext } from '../index'
import { RulesHook } from '../RulesHooks'
import { ConfirmModal } from '../../confirm_modal'
import { useConfirmModal } from '../../confirm_modal/useConfirmModal'

type PropsType = {
  id: number
  name: string
}

export const DeleteRuleButton: React.FC<PropsType> = (props) => {
  const Rules = React.useContext<RulesHook>(RulesContext)
  const confirmModalHooks = useConfirmModal()

  return (
    <>
      <Button onClick={confirmModalHooks.show} >
        ルール削除
    </Button>
      <ConfirmModal
        title="本当に削除しますか?"
        confirmModalHooks={confirmModalHooks}
        onConfirm={() => {
          Rules.deleteRule(props.id)
        }}
        onCancel={() => { }}
      >
        {props.name}
      </ConfirmModal>
    </>
  )
}
