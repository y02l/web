import * as React from 'react'


import { DeleteRuleButton } from './deleteRuleButton'
import { CopyRuleButton } from './copyRuleButton'
import { EditRuleForm } from './editRuleForm'
import { Rule, RulesHook } from '../RulesHooks'
import { RulesContext } from '../index'
import { Costs } from './costs'

type RuleProps = {
  rule: Rule
}

export const RuleView: React.FC<RuleProps> = props => {
  const Rules = React.useContext<RulesHook>(RulesContext)
  const rule: Rule = props.rule

  return (
    <div>
      <EditRuleForm
        name={rule.name}
        handleApply={(name) => { Rules.updateRule(rule.id, { name }) }} />
      <DeleteRuleButton id={rule.id} name={rule.name} />
      <CopyRuleButton id={rule.id} />
      <Costs ruleid={rule.id} costs={rule.costs} />
    </div>
  )
}
