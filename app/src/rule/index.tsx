import * as React from 'react'

import { Accordion, Card, Button } from 'react-bootstrap'

import { useRules, RulesHook } from './RulesHooks'
import { ApplyRuleButton } from './applyRuleButton'
import { AddRuleButton } from './addRuleButton'
import { RuleView } from './rule'

export const RulesContext = React.createContext({})

export const RulePage = () => {
  const Rules: RulesHook = useRules()

  React.useEffect(() => { Rules.updateRulesFromServer() }, [])

  return (
    <>
      <RulesContext.Provider value={Rules}>
        <ApplyRuleButton />
        <AddRuleButton />
        <Accordion>
          {Rules.rules.map((e, i) => (
            <Card key={e.id}>
              <Card.Header style={{ paddingTop: 1, paddingBottom: 1 }}>
                <Accordion.Toggle eventKey={i + 1} as={Button} variant="link">
                  {e.name}
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey={i + 1}>
                <RuleView rule={e} />
              </Accordion.Collapse>
            </Card>
          ))}
        </Accordion>
      </RulesContext.Provider>
    </>
  )
}
