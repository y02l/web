import * as React from 'react'

import axios from 'axios'
import _ from 'lodash'

export interface Cost {
  id: number
  name: string
  cost: number
}

export interface Rule {
  id: number
  name: string
  cost_capacity: boolean
  max_capacity: number
  costs: Cost[]
}

export interface RulesHook {
  rules?: Rule[]
  addRule?: (rule: Partial<Rule>) => void
  deleteRule?: (id: number) => void
  updateRule?: (id: number, override: Partial<Rule>) => void
  copyRule?: (id: number) => void
  addCost?: (ruleid: number, cost?: Partial<Cost>) => void
  deleteCost?: (ruleid: number, costid: number) => void
  updateCost?: (ruleid: number, costid: number, override: Partial<Cost>) => void
  postRules?: () => Promise<void>
  updateRulesFromServer?: () => Promise<void>
}

interface RuleDataForServer {
  name: string
  cost_capacity: boolean
  max_capacity: number
  costs: Cost[]
}

export const useRules = (): RulesHook => {
  const [rules, setRules] = React.useState<Rule[]>([])

  // カードコストをコストでソートする
  const [prevRules, setPrevRules] = React.useState<Rule[]>([])
  React.useEffect(() => {
    if (!_.isEqual(rules, prevRules)) {
      setRules(
        rules.map((rule) => (
          {
            ...rule,
            costs: rule.costs.sort((a, b) => (b.cost - a.cost))
          }
        ))
      )
      setPrevRules(rules)
    }
  }, [rules])

  const getAvailableId = (hasIdList: { id: number }[]): number => {
    return hasIdList.length === 0 ? 0 : hasIdList.reduce((a, c) => Math.max(a, c.id), 0) + 1
  }

  const addRule = (rule: Partial<Rule>) => {
    setRules((prev) => {
      return [...prev, {
        name: '新しいルール',
        cost_capacity: true,
        max_capacity: 10,
        costs: [],
        ...rule,
        id: getAvailableId(prev)
      }]
    })
  }

  const updateRule = (id: number, override: Partial<Rule>) => {
    setRules((prev) => {
      return [...prev.filter((e) => (e.id !== id)), {
        ...(prev.filter((e) => (e.id === id))[0]), ...override
      }]
    })
  }

  const deleteRule = (id: number) => {
    setRules((prev) => (prev.filter(
      (e) => (e.id !== id)
    )))
  }

  const copyRule = (id: number) => {
    setRules((prev) => {
      const src = prev.filter((e) => (e.id === id))[0]
      const dest = { ...src, name: src.name + 'のコピー', id: getAvailableId(prev) }
      return [...prev, dest]
    }
    )
  }

  const addCost = (ruleid: number, cost: Partial<Cost>) => {
    setRules((prev) => {
      const rule = prev.filter((e) => (e.id === ruleid))[0]
      const costs = rule.costs
      const newCost: Cost = {
        name: '新しいカード',
        cost: 9,
        ...cost,
        id: getAvailableId(costs)
      }
      const newCosts = [...costs, newCost]
      return prev.map((e) => (
        e.id === ruleid ? { ...e, costs: newCosts } : e
      ))
    })
  }

  const deleteCost = (ruleid: number, costid: number) => {
    setRules((prev) => {
      const rule = prev.filter((e) => (e.id === ruleid))[0]
      const costs = rule.costs
      const newCosts = costs.filter((e) => (e.id !== costid))
      return prev.map((e) => (
        e.id === ruleid ? { ...e, costs: newCosts } : e
      ))
    })
  }

  const updateCost = (ruleid: number, costid: number, override: Partial<Cost>) => {
    setRules((prev) => {
      const rule = prev.filter((e) => (e.id === ruleid))[0]
      const costs = rule.costs
      const newCosts = costs.map((e) => (e.id === costid ? {
        ...e, ...override
      } : e))
      const rulesCopy = [...prev]
      const newRules = rulesCopy.map((e) => (
        e.id === ruleid ? { ...e, costs: newCosts } : e
      ))
      return newRules
    })
  }

  const postRules = async () => {
    const data: RuleDataForServer[] = rules.map((e) => ({ ...e }))
    await axios.post('/api/deck-rule/rules/all', data)
  }

  const updateRulesFromServer = async () => {
    const data: RuleDataForServer[] = (await axios.get('/api/deck-rule/rules')).data
    setRules(
      data.map((e, index) => (
        { ...e, id: index }
      ))
    )
  }

  return { rules, addRule, deleteRule, updateRule, copyRule, addCost, deleteCost, updateCost, postRules, updateRulesFromServer }
}
