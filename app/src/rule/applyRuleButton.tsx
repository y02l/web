import * as React from 'react'

import { Button } from 'react-bootstrap'

import { RulesContext } from './index'
import { RulesHook } from './RulesHooks'

export const ApplyRuleButton = () => {
  const Rules = React.useContext<RulesHook>(RulesContext)
  const [loadingApply, setLoadingApply] = React.useState(false)

  React.useEffect(() => {
    if (loadingApply) {
      Rules.postRules().then(() => {
        setLoadingApply(false)
      })
    }
  }, [loadingApply])

  return (
    <Button
      variant="primary"
      disabled={loadingApply}
      onClick={() => setLoadingApply(true)}
    >
      {loadingApply ? "反映中..." : "編集したらこのボタンを押してください サーバに反映します"}
    </Button>
  )
}
