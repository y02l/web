import * as React from 'react'
import { LinkContainer } from "react-router-bootstrap"
import { Navbar, Nav } from 'react-bootstrap'

export const Header = () => {
  return (
    <>
      <Navbar collapseOnSelect expand='lg' bg='dark' variant='dark'>
        <Navbar.Brand href='/'>02環境リンクス</Navbar.Brand>
        <Navbar.Toggle aria-controls='responsive-navbar-nav' />
        <Navbar.Collapse id='responsive-navbar-nav'>
          <Nav className='mr-auto'>
            <LinkContainer to="/checker">
              <Nav.Link>Deck Checker</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/rules">
              <Nav.Link>Deck Rules</Nav.Link>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  )
}
